package com.kraftwerking.spring.test;

import org.springframework.stereotype.Component;

@Component
public class Camera implements Photosnapper, Machine, ICamera {
	
	public Camera() {
		// TODO Auto-generated constructor stub
		System.out.println("Hello from Camera constructor!");
	}
	
	/* (non-Javadoc)
	 * @see com.kraftwerking.spring.test.ICamera#snap()
	 */
	@Override
	public void snap() throws Exception {
		System.out.println("SNAP!");
		throw new Exception("Bye bye!");
	}

	/* (non-Javadoc)
	 * @see com.kraftwerking.spring.test.ICamera#snapNighttime()
	 */
	@Override
	public void snapNighttime() {
		System.out.println("SNAP! Nighttime");
	}
	
	/* (non-Javadoc)
	 * @see com.kraftwerking.spring.test.ICamera#snap(int)
	 */
	@Override
	public void snap(int exposure) {
		System.out.println("SNAP! Exposure: " + exposure);
	}
	
	/* (non-Javadoc)
	 * @see com.kraftwerking.spring.test.ICamera#snap(java.lang.String)
	 */
	@Override
	public void snap(String name) {
		System.out.println("SNAP! Name: " + name);
	}
}
