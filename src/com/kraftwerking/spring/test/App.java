package com.kraftwerking.spring.test;

import org.springframework.context.support.ClassPathXmlApplicationContext;


public class App {
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				"com/kraftwerking/spring/test/beans.xml");
		Object obj = context.getBean("camera");
		System.out.println("Class of camera bean " + obj.getClass());
		System.out.println(obj instanceof Photosnapper);

		ICamera camera = (ICamera) context.getBean("camera");

		try{
			camera.snap();

		} catch(Exception ex) {
			System.out.println("Caught exception " + ex);
		}
;
		context.close();
	}

	
}
    