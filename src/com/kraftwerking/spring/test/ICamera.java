package com.kraftwerking.spring.test;

public interface ICamera {

	public abstract void snap() throws Exception;

	public abstract void snapNighttime();

	public abstract void snap(int exposure);

	public abstract void snap(String name);

}